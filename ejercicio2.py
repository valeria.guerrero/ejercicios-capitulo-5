# Escribe otro programa que pida una lista de números como la anterior
# y al ﬁnal muestre por pantalla el máximo y mínimo de los números, en vez de la media
# --autora-- ="Valeria Guerrero"
# --email-- ="valeria.guerrero@unl.edu.ec"
total=0
lista = []
suma=0
while True:
      valor=input("Ingresa un número entero(o ´fin´para terminar):")
      lista.append(valor)

      if valor.lower() in 'fin':
          break
      try:
          suma+=float(valor)
          total+=1
          max1=max(lista)
          min1=min(lista)

      except ValueError:
          print("Valor ingresado incorrecto. Ingrese el valor denuevo..")

          print("La suma es->:", suma)
          print("Haz ingresado->:",total,"números")
          print("Lista de números->:", lista)
          print("El número mayor ingresado es->, float(max1)")
          print("El número menor ingresado es->,float(min1)")