#  Escribe un programa que lea repetidamente números hasta que el usuario introduzca “ﬁn”
#  muestra por pantalla el total, la cantidad de números y la media de esos números.
#   Si el usuario introduce cualquier otra cosa que no sea un número
#   detecta su fallo usando try y except,
#   muestra un mensaje de error y pasa al número siguiente
# --autora-- ="Valeria Guerrero"
# --email-- ="valeria.guerrero@unl.edu.ec"
total=0
suma=0
while True:
      valor=input("Itroduce un número entero(o ´fin´para terminar):")
      if valor.lower()in "fin":
         break
      try:
          suma += int(valor)
          total += 1
          media = suma / total
      except ValueError:
          print("Valor ingresado incorrecto. Introduzca el valor nuevamente..")

print("La suma es->:", suma)
print("Haz ingresado->:", total,"números")
print("La media de los valores es->:",suma/total)
